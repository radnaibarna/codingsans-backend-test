import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/local";


mongoose.connect(uri, (err: any) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log("Successfully Connected!");
    }
  });


  export interface userdb extends mongoose.Document {
    id: string;
    name: string;
    password: string
  }


  export const UserSchema = new mongoose.Schema({
    
   

    id: { type: String, required: true },
    name:  { type: String, required: true },
    password:  { type: String, required: true },
  });
  
  const User = mongoose.model<userdb>("USer", UserSchema);
  export default User;